const panelSetup      = document.querySelector('.setup-panel');
const panelPlayer     = document.querySelector('.player-panel');
const panelScoreboard = document.querySelector('.scoreboard-panel');
const btnStart        = document.querySelector('#btn-start');
const btnNext         = document.querySelector('#btn-next');
const btnReset        = document.querySelector('#reset');
const btnRenew       = document.querySelector('#renew');
const controlPlayers  = document.querySelector('#players-score');

var playerList = document.querySelector('#player-names');
var players = 2;
var isReady = false;
games = JSON.parse(localStorage.getItem('games')) || [];

window.onload = function() {
    panelPlayer.style.display = "none";
    panelScoreboard.style.display = "none";   

    localStorage.clear();
}

if(isReady){
    window.addEventListener('load', () => {    
        displayScoreboard();
    })
}

btnStart.addEventListener('click', () => {
    players = document.querySelector('#players').value;
    createPlayer(players);
    
    panelSetup.style.display = "none";
    panelPlayer.style.display = "block";
})

btnNext.addEventListener('click', () => {    
    displayScoreboard();
    panelPlayer.style.display = "none";
    panelScoreboard.style.display = "block";
    isReady = true;
})

btnReset.addEventListener('click', () => {  })
btnRenew.addEventListener('click', () => {  })

function createPlayer(num){    
    let playerItems = "";
    for(var i = 0; i < num; i++){
        playerItems += `
        <div class="player-${i+1} list-item">
        <label for="player-name">Player ${i+1}</label>
        <input type="text" name="player-name" class="input-name">
        </div>
        `;
    }
    playerList.innerHTML = playerItems;
};

function displayScoreboard(){
    var names = document.querySelectorAll('.input-name');

    // for(var j = 0; j < names.length; j++){
    //     var playerName = names[j].value;
        
    //     const player = {
    //         name: playerName,
    //         score: 0
    //     }

    //     games.push(player);
    //     localStorage.setItem('games', JSON.stringify(games));
    // }
    games.foreach( () => {
        var playerName = this.value;
        
        const player = {
            name: playerName,
            score: 0
        }

        games.push(player);
        localStorage.setItem('games', JSON.stringify(games));

        const playerItem = document.createElement('div');
        playerItem.classList.add('player-score');

        const title = document.createElement('h1');
        const input = document.createElement('input');
        const control = document.createElement('div');
        const up = document.createElement('button');
        const down = document.createElement('button');

        title.classList.add('player-name');
        control.classList.add('score-control');
        up.classList.add('up');
        down.classList.add('down');

        input.innerHTML = `<input type="text" class="score" name="player1-score" value="${player.score}" disabled>`;
        up.innerHTML = '+1';
        down.innerHTML = '-1';

        control.appendChild(up);
        control.appendChild(down);
        playerItem.appendChild(title);
        playerItem.appendChild(input);
        playerItem.appendChild(control);

        controlPlayers.appendChild(playerItem);

        up.addEventListener('click', e => {
            const score = playerItem.querySelector('score');
            score.focus();
            score +=1;
            player.score = score;
            localStorage.setItem('games', JSON.stringify(games));
        })

        down.addEventListener('click', e => {            
            const score = playerItem.querySelector('score');
            score.focus();
            score -=1;
            player.score = score;
            localStorage.setItem('games', JSON.stringify(games));            
        })
    })
};
