const panelSetup      = document.querySelector('.setup-panel');
const panelPlayer     = document.querySelector('.player-panel');
const panelScoreboard = document.querySelector('.scoreboard-panel');
const btnStart        = document.querySelector('#btn-start');
const btnNext         = document.querySelector('#btn-next');
const btnReset        = document.querySelector('#reset');

var playerList = document.querySelector('#player-names');
var players = 2;
games = JSON.parse(localStorage.getItem('player')) || [];

window.onload = function() {
    panelPlayer.style.display = "none";
    panelScoreboard.style.display = "none";    
}

btnStart.addEventListener('click', () => {
    players = document.querySelector('#players').value;
    createPlayer(players);
    
    panelSetup.style.display = "none";
    panelPlayer.style.display = "block";
})

btnNext.addEventListener('click', () => {
    var names = document.querySelectorAll('.input-name');

    for(var j = 0; j < names.length; j++){
        var playerName = names[j].value;
        if (playerName == ""){playerName = `Player ${j+1}`};
        
        const player = {
            name: playerName,
            score: 0
        }

        games.push(player);
        localStorage.setItem('games', JSON.stringify(games));
    }
    // createScoreboard();

    panelPlayer.style.display = "none";
    panelScoreboard.style.display = "block";
})

function createPlayer(num){    
    let playerItems = "";
    for(var i = 0; i < num; i++){
        playerItems += `
        <div class="player-${i+1} list-item">
        <label for="player-name">Player ${i+1}</label>
        <input type="text" name="player-name" class="input-name">
        </div>
        `;
    }
    playerList.innerHTML = playerItems;
};

function createScoreboard(){
    const playersScore = document.querySelector('#players-score');
    playersScore.innerHTML = '';

    games.forEach(player => {
        const playerItem = document.createElement('div');
        playerItem.classList.add('player-score');

        const title = document.createElement('h1');
        const input = document.createElement('input');
        const control = document.createElement('div');
        const up = document.createElement('button');
        const down = document.createElement('button');

        input.attr('disabled', 'disabled');
        title.classList.add('player-score')
    });
}
